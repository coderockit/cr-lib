module gitlab.com/coderockit/cr-lib

go 1.19

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/mattn/go-sqlite3 v1.14.15 // indirect
)
