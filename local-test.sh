#!/bin/sh

go test -v -coverprofile=coverage.out ./main ./coderockit ./coderockit/gizmo
# To see results in html: go tool cover -html=coverage.out