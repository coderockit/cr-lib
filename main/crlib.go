package main

/*
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
typedef struct payloadFile {
    int64_t digest;
    bool exists;
    unsigned char filePath[5000];
	int filePathSize;
} payloadFile;
*/
import "C"
import (
	"fmt"

	"gitlab.com/coderockit/cr-lib/coderockit"
	"gitlab.com/coderockit/cr-lib/coderockit/model"
)

func init() {
	// this init gets called automatically before the main() function
	//fmt.Println("crlib init called before main")
}

// type LibPayloadFile model.PayloadFile

// type PayloadFile struct {
// 	digest   int64
// 	filePath []byte
// 	exists   bool
// }

/* NOTE: DO NOT allow a space infront of the keyword 'export' on the next line below!!!!!!!!!!!!!!!!!!!! */

//export DoubleIt
func DoubleIt(x int) int {
	fmt.Println(x)
	return x * 2
}

// Free native memory which we allocated in cgo such as en using C.CString.
// This is needed so the caller can safely release the memory allocated by our code.
//
//export free_pointer
// func free_pointer(pointer unsafe.Pointer) {
// 	// println("freeing pointer from cgo") // to prove it works
// 	C.free(pointer)
// }

//export StopLibrary
func StopLibrary() int {
	returnCode := model.CloseCHandles()
	var panicChan chan bool
	close(panicChan)
	return returnCode
}

/* NOTE: DO NOT allow a space infront of the keyword 'export' on the next line below!!!!!!!!!!!!!!!!!!!! */

//export ScanForFilesWithPayloads
func ScanForFilesWithPayloads(
	baseDir string, crDir string,
	logLevel int, exclSubStrs []string,
	inclSubStrs []string, exclRegExps []string,
	inclRegExps []string, folders []string) int {
	return coderockit.FindFilesWithPayloads(baseDir, crDir, logLevel, exclSubStrs, inclSubStrs, exclRegExps, inclRegExps, folders)
}

/* NOTE: DO NOT allow a space infront of the keyword 'export' on the next line below!!!!!!!!!!!!!!!!!!!! */

//export GetScannedFilesList
func GetScannedFilesList(
	baseDir string, crDir string,
	logLevel int, exclSubStrs []string,
	inclSubStrs []string, exclRegExps []string,
	inclRegExps []string, lastPayloadFileDigest int64) C.uintptr_t {
	result := coderockit.GetScannedFilesList(baseDir, crDir, logLevel, exclSubStrs, inclSubStrs, exclRegExps, inclRegExps, lastPayloadFileDigest)
	return C.uintptr_t(result)
}

//export GetNextPayloadFile
func GetNextPayloadFile(handle C.uintptr_t, file *C.struct_payloadFile) int {
	// C.GoBytes(unsafe.Pointer, C.int) []byte
	// return model.GetNextPart(uint64(handle), model.HandleType(handleType))
	var exists bool
	var digest int64
	var filepath string
	chandle := uint64(handle)
	if model.GetCgoHandleType(chandle) == model.PayloadFileArray {
		foundOne := model.GetNextPayloadFile(chandle, &digest, &exists, &filepath)
		if foundOne == 0 {
			file.digest = C.int64_t(digest)
			file.exists = C.bool(exists)
			filepathbytes := []byte(filepath)
			for index, b := range filepathbytes {
				file.filePath[index] = C.uchar(b)
			}
			file.filePathSize = C.int(len(filepathbytes))
		}
		return foundOne
	}
	return 2
}

func main() {
	// fmt.Println("crlib main called after init")
}
