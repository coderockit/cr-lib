#!/bin/sh

# go tool dist list

# CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o build/linux-amd64/libcoderockit.so -buildmode=c-shared crlib/crlib.go
# CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o build/linux-amd64/libcoderockit.a -buildmode=c-archive crlib/crlib.go

# CGO_ENABLED=1 GOOS=darwin GOARCH=arm64 go build -o build/darwin-arm64/libcoderockit.dylib -buildmode=c-shared crlib/crlib.go
# CGO_ENABLED=1 GOOS=darwin GOARCH=arm64 go build -o build/darwin-arm64/libcoderockit.a -buildmode=c-archive crlib/crlib.go

rm -rf build/*
BUILD_NUM=`cat buildnum`
mkdir -p build/darwin-arm64/$BUILD_NUM
go build -o build/darwin-arm64/$BUILD_NUM/libcoderockit.dylib -buildmode=c-shared main/crlib.go
go build -o build/darwin-arm64/$BUILD_NUM/libcoderockit.a -buildmode=c-archive main/crlib.go
BUILD_NUM=$((BUILD_NUM+1))
echo $BUILD_NUM > buildnum
