package gizmo

const PayloadBeginTagDelimiter string = "|||"
const PayloadBeginTag string = PayloadBeginTagDelimiter + "coderockit:/@"
const PayloadEndTag string = "[[[coderockit:/@]]]"
