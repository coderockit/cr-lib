package gizmo

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"regexp"

	"github.com/cespare/xxhash/v2"
)

func Last[E any](s []E) (E, bool) {
	if len(s) == 0 {
		var zero E
		return zero, false
	}
	return s[len(s)-1], true
}

func CompileRegExps(regexs []string) []regexp.Regexp {
	regexps := make([]regexp.Regexp, len(regexs), len(regexs)) // [len(regexs)]regexp.Regexp
	for i, regex := range regexs {
		r, _ := regexp.Compile(regex)
		regexps[i] = *r
	}
	return regexps
}

func XxHash(toHash string) int64 {
	return int64(xxhash.Sum64String(toHash))
}

func XxHashBytes(toHash []byte) int64 {
	return int64(xxhash.Sum64(toHash))
}

func FileExists(filepath string) (bool, error) {
	return pathExists(filepath, false)
}

func DirExists(dirpath string) (bool, error) {
	return pathExists(dirpath, true)
}

func pathExists(path string, needDir bool) (bool, error) {
	if fInfo, err := os.Stat(path); err == nil {
		if needDir {
			return fInfo.IsDir(), nil
		} else {
			return !fInfo.IsDir(), nil
		}
	} else if errors.Is(err, os.ErrNotExist) {
		return false, nil
	} else {
		return false, err
	}
}

func ReflectToType(reflectType reflect.Type) int {
	fmt.Printf("ReflectType is: %v", reflectType.Kind().String())
	return 1
}
