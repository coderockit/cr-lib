package gizmo

import (
	"database/sql"
)

func executeDDL(db *sql.DB) {
	log := GetLogger()
	_, err := db.Exec("CREATE TABLE IF NOT EXISTS payload_files(digest INTEGER NOT NULL PRIMARY KEY, filepath TEXT UNIQUE NOT NULL)")
	if err != nil {
		log.Debugf("%s", err)
	}
}
