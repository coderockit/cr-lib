package gizmo

import (
	"database/sql"
	"os"
	"path/filepath"
	"sync"

	sqlite3 "github.com/mattn/go-sqlite3"
)

const dbFolderName string = "payloadDB"
const dbFileName string = "payload.db"

func init() {
	// init the badger db here
	// fmt.Println("db.go init function")
	sql.Register("sqlite3_coderockit",
		&sqlite3.SQLiteDriver{
			ConnectHook: func(conn *sqlite3.SQLiteConn) error {
				sqlite3conn = append(sqlite3conn, conn)
				conn.RegisterUpdateHook(func(op int, db string, table string, rowid int64) {
					switch op {
					case sqlite3.SQLITE_INSERT:
						// fmt.Printf("Notified of insert on db %s table %s rowid %d\n", db, table, rowid)
					}
				})
				return nil
			},
		})
}

// dbLockInit - the Mutex / Lock to guarantee only a code block is
// accessible ONLY by 1 thread / routine at a time.
var dbLockInit sync.Mutex

var singletonDB *sql.DB
var sqlite3conn = []*sqlite3.SQLiteConn{}

func GetDBFolder(crDir string) string {
	return filepath.Join(crDir, dbFolderName)
}

func GetDB() *sql.DB {
	return singletonDB
}

func GetConnection() *sqlite3.SQLiteConn {
	return sqlite3conn[0]
}

func IdempotentOpenDB(dbFolder string) error {
	dbLockInit.Lock()
	defer dbLockInit.Unlock()

	if singletonDB == nil {
		// log.Errorf("The crDir is: %s and the length is: %d", crDir, len(crDir))
		os.MkdirAll(dbFolder, os.ModePerm)
		db, err := sql.Open("sqlite3_coderockit", filepath.Join(dbFolder, dbFileName))
		singletonDB = db
		executeDDL(db)
		return err
	}
	return nil
}

func CloseDB(db *sql.DB) error {
	err := db.Close()
	singletonDB = nil
	sqlite3conn = []*sqlite3.SQLiteConn{}
	return err
}
