/*
 * Copyright 2018 Dgraph Labs, Inc. and Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gizmo

import (
	"io"
	"log"
	"os"
	"sync"
)

// Logger is implemented by any logging system that is used for standard logs.
type Logger interface {
	Errorf(string, ...interface{})
	Warningf(string, ...interface{})
	Infof(string, ...interface{})
	Debugf(string, ...interface{})
	SetLevel(newLevel LoggingLevel)
}

// loggerLockInit - the Mutex / Lock to guarantee only a code block is
// accessible ONLY by 1 thread / routine at a time.
var loggerLockInit sync.Mutex

// singletonLogger - the 1 and ONLY 1 instance of type Logger.
var singletonLogger Logger
var singletonLogLevel LoggingLevel

type LoggerOpts struct {
	// optional arguments
	Out io.Writer
}

// GetLoggingLevel - return the singletonLogLevel instance.
func GetLoggingLevel() LoggingLevel {
	return singletonLogLevel
}

// GetLogger - return the singletonLogger instance.
func GetLogger() Logger {
	return singletonLogger
}

// IdempotentInitLogger - initialize the singletonLogger instance, every invocation has the same side effect.
func IdempotentInitLogger(level LoggingLevel, opts *LoggerOpts) {
	loggerLockInit.Lock()
	defer loggerLockInit.Unlock()

	if singletonLogger == nil {
		out := io.Writer(os.Stderr)
		if opts != nil && opts.Out != nil {
			out = opts.Out
		}
		singletonLogLevel = level
		singletonLogger = defaultLogger(level, out)
	}
}

type LoggingLevel int

const (
	ERROR LoggingLevel = iota
	WARN
	INFO
	DEBUG
)

type defaultLog struct {
	*log.Logger
	level LoggingLevel
}

func defaultLogger(level LoggingLevel, out io.Writer) *defaultLog {
	return &defaultLog{Logger: log.New(out, "coderockit ", log.LstdFlags), level: level}
}

func (l *defaultLog) SetLevel(newLevel LoggingLevel) {
	l.level = newLevel
}

func (l *defaultLog) Errorf(f string, v ...interface{}) {
	if l.level >= ERROR {
		l.Printf("ERROR: "+f, v...)
	}
}

func (l *defaultLog) Warningf(f string, v ...interface{}) {
	if l.level >= WARN {
		l.Printf("WARN: "+f, v...)
	}
}

func (l *defaultLog) Infof(f string, v ...interface{}) {
	if l.level >= INFO {
		l.Printf("INFO: "+f, v...)
	}
}

func (l *defaultLog) Debugf(f string, v ...interface{}) {
	if l.level >= DEBUG {
		l.Printf("DEBUG: "+f, v...)
	}
}
