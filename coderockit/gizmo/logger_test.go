package gizmo

import (
	"bytes"
	"io"
	"strings"
	"testing"
)

func TestLogger(t *testing.T) {
	// actualString := ReturnGeeks()
	// expectedString := "geeks"
	// if actualString != expectedString {
	// 	t.Errorf("Expected String(%s) is not same as"+
	// 		" actual string (%s)", expectedString, actualString)
	// }

	out := bytes.NewBufferString("")
	IdempotentInitLogger(LoggingLevel(0), &LoggerOpts{Out: io.Writer(out)})
	log := GetLogger()

	errStmt := "only error"
	log.Errorf(errStmt)
	logMsg := out.String()
	if !strings.Contains(logMsg, errStmt+"\n") {
		t.Fatalf(`log.Errorf("%q") = %q, %v, want match for %#q`, errStmt, logMsg, "log statement does not match", errStmt)
	}

	debugStmt := "only debug"
	log.Debugf(debugStmt)
	logMsg = out.String()
	if strings.Contains(logMsg, debugStmt+"\n") {
		t.Fatalf(`log.Debugf(%q) = %q, %v, want match for %#q`, debugStmt, logMsg, "log statement does not match", debugStmt)
	}

	log.SetLevel(LoggingLevel(3))
	log.Infof("only info")

	log.Warningf("only warning")
}

func TestLoggerNil(t *testing.T) {
	// actualString := ReturnGeeks()
	// expectedString := "geeks"
	// if actualString != expectedString {
	// 	t.Errorf("Expected String(%s) is not same as"+
	// 		" actual string (%s)", expectedString, actualString)
	// }

	IdempotentInitLogger(LoggingLevel(0), nil)
	log := GetLogger()
	log.Debugf("only debug")
}

func TestLoggerEmptyOpts(t *testing.T) {
	// actualString := ReturnGeeks()
	// expectedString := "geeks"
	// if actualString != expectedString {
	// 	t.Errorf("Expected String(%s) is not same as"+
	// 		" actual string (%s)", expectedString, actualString)
	// }

	IdempotentInitLogger(LoggingLevel(0), &LoggerOpts{})
	log := GetLogger()
	log.Debugf("only debug")
}
