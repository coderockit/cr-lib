package coderockit

import (
	"bufio"
	"os"
	"path/filepath"
	"regexp"
	"runtime/cgo"
	"strings"

	"gitlab.com/coderockit/cr-lib/coderockit/gizmo"
	"gitlab.com/coderockit/cr-lib/coderockit/model"
)

func init() {

}

func GetScannedFilesList(
	baseDir string, crDir string,
	logLevel int, exclSubStrs []string,
	inclSubStrs []string, exclRegExps []string,
	inclRegExps []string, lastPayloadFileDigest int64) cgo.Handle {

	gizmo.IdempotentInitLogger(gizmo.LoggingLevel(logLevel), nil)
	log := gizmo.GetLogger()

	// NOTE: Skip all files and directories containing the hidden .coderockit folder followed by the coderockit.realm and payloadDB
	exclSubStrs = append(exclSubStrs, filepath.Join(".coderockit", "coderockit.realm"))
	exclSubStrs = append(exclSubStrs, filepath.Join(".coderockit", "payloadDB"))

	log.Debugf("Base directory is: %s", baseDir)
	log.Debugf("CodeRockIT directory is: %s", crDir)
	log.Debugf("Exclude substrings is: %s", exclSubStrs)
	log.Debugf("Include substrings is: %s", inclSubStrs)
	log.Debugf("Exclude regular expressions is: %s", exclRegExps)
	log.Debugf("Include regular expressions is: %s", inclRegExps)

	// compiledExclRE := gizmo.CompileRegExps(exclRegExps)
	// compiledInclRE := gizmo.CompileRegExps(inclRegExps)

	// initialize the database AND defer to the end of the function the closing of the database
	dberr := gizmo.IdempotentOpenDB(gizmo.GetDBFolder(crDir))
	if dberr != nil {
		log.Errorf("%s", dberr)
	}
	defer gizmo.CloseDB(gizmo.GetDB())

	return model.GetPayloadFiles(lastPayloadFileDigest)
}

func containsPayloadTag(scanFile *os.File) bool {
	fileScanner := bufio.NewScanner(scanFile)
	fileScanner.Split(bufio.ScanLines)

	foundPayload := false
	foundBeginning := false
	for fileScanner.Scan() {
		line := fileScanner.Text()
		beginIndex := strings.Index(line, gizmo.PayloadBeginTag)
		if foundBeginning ||
			(beginIndex != -1 &&
				strings.Contains(line[beginIndex+len(gizmo.PayloadBeginTag):], gizmo.PayloadBeginTagDelimiter)) {
			if foundBeginning {
				endIndex := strings.Index(line, gizmo.PayloadEndTag)
				if endIndex != -1 {
					foundPayload = true
					break
				}
			} else {
				foundBeginning = true
			}
		}
	}
	return foundPayload
}

func findFilesWithPayloadsRecursively(nextEntry string, exclSubStrs []string,
	inclSubStrs []string, compiledExclRE []regexp.Regexp,
	compiledInclRE []regexp.Regexp) {
	log := gizmo.GetLogger()
	if includeFullPath(nextEntry, exclSubStrs, inclSubStrs, compiledExclRE, compiledInclRE) {
		newEntries := []string{}

		scanFile, ferr := os.Open(nextEntry)
		if ferr != nil {
			log.Errorf("%s", ferr)
			return
		}

		fInfo, fiErr := scanFile.Stat()
		if fiErr != nil {
			log.Errorf("%s", fiErr)
			return
		}
		fMode := fInfo.Mode()
		if fMode.IsDir() {
			files, err := scanFile.ReadDir(0)
			if err != nil {
				log.Errorf("%s", err)
				return
			}

			for _, dirEntry := range files {
				path := filepath.Join(nextEntry, dirEntry.Name())
				log.Debugf("Adding path to newEntries: %s", path)
				newEntries = append(newEntries, path)
			}
		} else if fMode&os.ModeSymlink == os.ModeSymlink {
			resolvedLink, lerr := os.Readlink(nextEntry) // TODO: Handle error here
			if lerr != nil {
				log.Errorf("%s", lerr)
				return
			}
			if !filepath.IsAbs(resolvedLink) { // Output of os.Readlink is OS-dependent...
				resolvedLink = filepath.Join(filepath.Dir(nextEntry), resolvedLink)
			}
			log.Debugf("Following symbolic link: %s", resolvedLink)
			newEntries = append(newEntries, resolvedLink)
		} else {
			//path := filepath.Join(nextEntry, fInfo.Name())
			log.Debugf("Checking file for payload tags: %s %d", nextEntry, fInfo.Size())

			// now process file that was found
			if containsPayloadTag(scanFile) {
				// add the file to the database index
				log.Infof("File %s has at least one payload tag", nextEntry)
				model.InsertPayloadFile(gizmo.XxHash(nextEntry), nextEntry)
			}

		}
		scanFile.Close()

		// now loop over paths that were found
		for _, newEntry := range newEntries {
			findFilesWithPayloadsRecursively(newEntry, exclSubStrs, inclSubStrs, compiledExclRE, compiledInclRE)
		}
	} else {
		log.Debugf("Excluding path: %s", nextEntry)
	}
}

func FindFilesWithPayloads(
	baseDir string, crDir string,
	logLevel int, exclSubStrs []string,
	inclSubStrs []string, exclRegExps []string,
	inclRegExps []string, folders []string) int {

	gizmo.IdempotentInitLogger(gizmo.LoggingLevel(logLevel), nil)
	log := gizmo.GetLogger()

	// NOTE: Skip all files and directories containing the hidden .coderockit folder followed by the coderockit.realm and payloadDB
	exclSubStrs = append(exclSubStrs, filepath.Join(".coderockit", "coderockit.realm"))
	exclSubStrs = append(exclSubStrs, filepath.Join(".coderockit", "payloadDB"))

	log.Debugf("Base directory is: %s", baseDir)
	log.Debugf("CodeRockIT directory is: %s", crDir)
	log.Debugf("Exclude substrings is: %s", exclSubStrs)
	log.Debugf("Include substrings is: %s", inclSubStrs)
	log.Debugf("Exclude regular expressions is: %s", exclRegExps)
	log.Debugf("Include regular expressions is: %s", inclRegExps)

	compiledExclRE := gizmo.CompileRegExps(exclRegExps)
	compiledInclRE := gizmo.CompileRegExps(inclRegExps)

	if len(folders) <= 0 {
		folders = append(folders, baseDir)
	}
	log.Debugf("Scanning folders: %s", folders)

	// initialize the database AND defer to the end of the function the closing of the database
	dberr := gizmo.IdempotentOpenDB(gizmo.GetDBFolder(crDir))
	if dberr != nil {
		log.Errorf("%s", dberr)
	}
	defer gizmo.CloseDB(gizmo.GetDB())

	// loop over each folder in folders and scan for payload tags
	for _, folder := range folders {
		findFilesWithPayloadsRecursively(folder, exclSubStrs, inclSubStrs, compiledExclRE, compiledInclRE)
	}

	return 0
}

func containsOneOfSubStrs(str string, substrs []string) bool {
	for _, substr := range substrs {
		if strings.Contains(str, substr) {
			return true
		}
	}
	if len(substrs) == 0 {
		return true
	}
	return false
}

func mathchesOneOfRegExps(str string, regexps []regexp.Regexp) bool {
	for _, regex := range regexps {
		if regex.MatchString(str) {
			return true
		}
	}
	if len(regexps) == 0 {
		return true
	}
	return false
}

func includeFullPath(fullpath string, exclSubStrs []string,
	inclSubStrs []string, compiledExclRE []regexp.Regexp,
	compiledInclRE []regexp.Regexp) bool {

	if (len(exclSubStrs) == 0 && len(compiledExclRE) == 0) ||
		(len(exclSubStrs) != 0 &&
			len(compiledExclRE) == 0 &&
			!containsOneOfSubStrs(fullpath, exclSubStrs)) ||
		(len(exclSubStrs) == 0 &&
			len(compiledExclRE) != 0 &&
			!mathchesOneOfRegExps(fullpath, compiledExclRE)) ||
		(len(exclSubStrs) != 0 &&
			len(compiledExclRE) != 0 &&
			!containsOneOfSubStrs(fullpath, exclSubStrs) &&
			!mathchesOneOfRegExps(fullpath, compiledExclRE)) {
		if (len(inclSubStrs) == 0 && len(compiledInclRE) == 0) ||
			(len(inclSubStrs) != 0 &&
				containsOneOfSubStrs(fullpath, inclSubStrs)) ||
			(len(compiledInclRE) != 0 &&
				mathchesOneOfRegExps(fullpath, compiledInclRE)) {
			// log := gizmo.GetLogger()
			// log.Debugf("Including full path: %s", fullpath)
			return true
		} else {
			return false
		}
	} else {
		return false
	}
}
