package model

/*
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
*/
import "C"
import (
	"database/sql"
	"fmt"
	"runtime/cgo"
	"strings"

	"gitlab.com/coderockit/cr-lib/coderockit/gizmo"
)

// type CPerson struct {
// 	ptr unsafe.Pointer
// }

// func (cp CPerson) CPersonToNative() *C.Person {
// 	p * C.person
// 	p.name = C.CString(cp.name)
// 	p.age = (C.int)(cp.age)
// 	return p
// }

// func NewCPerson(ptr unsafe.Pointer) CPerson {
// 	var cp CPerson
// 	p := (*C.Person)(ptr)
// 	cp.name = C.GoString(p.name)
// 	cp.age = (int)(p.age)
// 	return cp
// }

// func PrintPerson() {
// 	p := GetPerson()
// 	fmt.Println("name:", C.GoString(p.name))
// 	fmt.Println("age:", p.age)
// }

// func GetPerson() CPerson {
// 	p := NewCPerson(unsafe.Pointer(C.new_person()))
// 	p.name = C.CString("John Doe")
// 	p.age = 39
// 	return p
// }

// type PayloadFileList struct {
// 	Files  **C.struct_payloadFile
// 	Length int
// }

// type ListWrapper struct {
// 	List unsafe.Pointer
// }

// func (l *ListWrapper) PrintFiles() {
// 	fmt.Printf("here is the unsafe.Pointer l.List: %v\n", l.List)
// 	p := (*C.struct_PayloadFileList)(l.List)
// 	fmt.Printf("here is the PayloadFileList.file p.files: %v of size %d\n", p.files, p.n)
// 	f := *p.files
// 	fmt.Printf("here is the first PayloadFile *p.files: %d, %s, %v\n", f.Digest, C.GoString(f.FilePath), bool(f.Exists))
// 	// f = *p.files
// 	// fmt.Printf("here is the first PayloadFile *p.files: %d, %s, %v\n", f.Digest, C.GoString(f.FilePath), bool(f.Exists))
// }

func InsertPayloadFile(digest int64, filePath string) {
	log := gizmo.GetLogger()
	db := gizmo.GetDB()
	_, err := db.Exec(fmt.Sprintf("INSERT INTO payload_files VALUES(%d, '%s')", digest, filePath))
	if err != nil {
		if strings.Contains(fmt.Sprintf("%s", err), "UNIQUE constraint failed") {
			log.Infof("InsertPayloadFile: %s is already in the database -- %s", filePath, err)
		} else {
			log.Errorf("InsertPayloadFile: %s", err)
		}
	}
}

func GetNextPayloadFile(chandle uint64, digest *int64, exists *bool, filepath *string) int {
	cgoHandle := cgo.Handle(chandle)
	rows := cgoHandle.Value().(*sql.Rows)
	if rows.Next() {
		log := gizmo.GetLogger()
		// db := gizmo.GetDB()
		//f := (*C.struct_payloadFile)(C.malloc(C.size_t(unsafe.Sizeof(C.struct_payloadFile{}))))
		// var digest int64
		// var filePath string
		err := rows.Scan(digest, filepath)
		// f.digest = C.int64_t(digest)
		// f.filePath = C.CString(filePath)
		if err != nil {
			log.Errorf("GetPayloadFiles: %s", err)
		}
		*exists, err = gizmo.FileExists(*filepath)
		if err != nil {
			log.Errorf("GetPayloadFiles: %s", err)
		}
		// f.exists = C.bool(exists)
		//files = append(files, f)
		//returnFiles.Files[length] = f
		// length++
		return 0
	} else {
		cgoHandle.Delete()
		DeleteCHandle(chandle)
		rows.Close()
		return 1
	}
}

func GetPayloadFiles(lastPayloadFileDigest int64) cgo.Handle {
	log := gizmo.GetLogger()
	db := gizmo.GetDB()
	sqlStr := fmt.Sprintf("SELECT * FROM payload_files WHERE digest > %d ORDER BY digest DESC LIMIT 1000", lastPayloadFileDigest)
	log.Debugf("SQL query is: %s", sqlStr)
	rows, err := db.Query(sqlStr)
	// if rows != nil {
	// 	defer rows.Close()
	// }

	if err != nil || rows == nil {
		if err != nil {
			log.Errorf("GetPayloadFiles: %s", err)
		}
		return 0 // PayloadFileList{Files: nil, Length: 0}
	} else {
		//returnFiles := PayloadFileList{}
		// returnFiles.Files.digest = 2
		// returnFiles.Files.filePath = C.CString("path to file")
		// returnFiles.Files.exists = true

		// var filesSlice []*C.struct_PayloadFile

		// // aif := &C.struct_PayloadFile{}
		// aif := (*C.struct_PayloadFile)(C.malloc(C.sizeof_struct_PayloadFile)) // (*C.struct_PayloadFile)(C.malloc(C.size_t(unsafe.Sizeof(C.struct_PayloadFile{}))))

		// aif.Digest = C.int64_t(2)
		// aif.FilePath = C.CString("/path2")
		// aif.Exists = C.bool(true)
		// filesSlice = append(filesSlice, aif)

		// aif = (*C.struct_PayloadFile)(C.malloc(C.sizeof_struct_PayloadFile)) // (*C.struct_PayloadFile)(C.malloc(C.size_t(unsafe.Sizeof(C.struct_PayloadFile{}))))

		// aif.Digest = C.int64_t(3)
		// aif.FilePath = C.CString("/path3")
		// aif.Exists = C.bool(false)
		// filesSlice = append(filesSlice, aif)

		// // air := &C.struct_PayloadFileList{}
		// air := (*C.struct_PayloadFileList)(C.malloc(C.sizeof_struct_PayloadFileList)) // (*C.struct_PayloadFileList)(C.malloc(C.size_t(unsafe.Sizeof(C.struct_PayloadFileList{}))))
		// air.files = &filesSlice[0]
		// air.n = C.int(len(filesSlice))
		// // ptrAddr := *((*C.int64_t)(unsafe.Pointer(&air)))

		// //exportToFFI(ptrAddr)

		// length := 0
		// for rows.Next() {
		// 	//f := (*C.struct_payloadFile)(C.malloc(C.size_t(unsafe.Sizeof(C.struct_payloadFile{}))))
		// 	var digest int64
		// 	var filePath string
		// 	err = rows.Scan(&digest, &filePath)
		// 	// f.digest = C.int64_t(digest)
		// 	// f.filePath = C.CString(filePath)
		// 	if err != nil {
		// 		log.Errorf("GetPayloadFiles: %s", err)
		// 	}
		// 	_, err := gizmo.FileExists(filePath)
		// 	if err != nil {
		// 		log.Errorf("GetPayloadFiles: %s", err)
		// 	}
		// 	// f.exists = C.bool(exists)
		// 	//files = append(files, f)
		// 	//returnFiles.Files[length] = f
		// 	length++
		// }

		// returnFiles.Files = (*C.struct_payloadFile)(C.malloc(C.size_t(length+1) * C.size_t(unsafe.Sizeof(C.struct_payloadFile{}))))
		// returnFiles.Files[0] = f
		// returnFiles.Length = length
		// return returnFiles
		// return &ListWrapper{List: unsafe.Pointer(air)}
		return NewCgoHandle(rows, PayloadFileArray)
	}
}

// func GetPayloadFiles(lastPayloadFileDigest int64) []PayloadFile {
// 	log := gizmo.GetLogger()
// 	db := gizmo.GetDB()
// 	sqlStr := fmt.Sprintf("SELECT * FROM payload_files WHERE digest > %d ORDER BY digest DESC LIMIT 1000", lastPayloadFileDigest)
// 	log.Debugf("SQL query is: %s", sqlStr)
// 	rows, err := db.Query(sqlStr)
// 	if rows != nil {
// 		defer rows.Close()
// 	}

// 	if err != nil {
// 		log.Errorf("GetPayloadFiles: %s", err)
// 		return nil
// 	} else {
// 		files := []PayloadFile{}
// 		for rows.Next() {
// 			f := PayloadFile{}
// 			err = rows.Scan(&f.Digest, &f.FilePath)
// 			if err != nil {
// 				log.Errorf("GetPayloadFiles: %s", err)
// 			}
// 			f.Exists, err = gizmo.FileExists(f.FilePath)
// 			if err != nil {
// 				log.Errorf("GetPayloadFiles: %s", err)
// 			}
// 			files = append(files, f)
// 		}
// 		return files
// 	}
// }

// package person

// import (
// 	"fmt"
// )

// // #include "../structs.h"
// import "C"

// func PrintPerson() {
// 	p := GetPerson()
// 	fmt.Println("name:", C.GoString(p.name))
// 	fmt.Println("age:", p.age)
// }

// func GetPerson() *C.Person {
// 	p := C.new_person()
// 	p.name = C.CString("John Doe")
// 	p.age = 39
// 	return p
// }

// package employee

// import (
// 	"fmt"

// 	"github.com/14rcole/cgo-examples/structs/person"
// )

// // #include "../structs.h"
// import "C"

// type employee struct {
// 	name    string
// 	age     int
// 	title   string
// 	company string
// }

// func PrintEmployee() {
// 	e := GetEmployee(person.GetPerson())
// 	fmt.Println("name:", e.name)
// 	fmt.Println("age:", e.age)
// 	fmt.Println("title:", e.title)
// 	fmt.Println("company:", e.company)
// }

// func GetEmployee(p *C.Person) employee {
// 	var e employee
// 	e.name = C.GoString(p.name)
// 	e.age = (int)(p.age)
// 	e.title = "Software Engineer"
// 	e.company = "Some Cool Company, Inc."

// 	return e
// }

// package person

// import (
// 	"fmt"
// )

// // #include "../structs.h"
// import "C"

// type CPerson struct {
//     ptr unsafe.Pointer
// }

// func (cp CPerson) CPersonToNative() *C.Person {
//     return cp.ptr
// }

// func NewCPerson(p unsafe.Pointer) CPerson {
//     var cp CPerson
//     cp.ptr = p
//     return CPerson
// }

// func PrintPerson() {
// 	p := GetPerson()
// 	fmt.Println("name:", C.GoString(p.name))
// 	fmt.Println("age:", p.age)
// }

// func GetPerson() CPerson {
// 	p := NewCPerson(unsafe.Pointer(C.new_person()))
// 	p.name = C.CString("John Doe")
// 	p.age = 39
// 	return p
// }

// var name := C.GoString(((*C.Person)(cp.CPersonToNative())).name)

// package person

// import (
// 	"fmt"
// )

// // #include "../structs.h"
// import "C"

// type CPerson struct {
//     ptr unsafe.Pointer
// }

// func (cp CPerson) CPersonToNative() *C.Person {
//     p *C.person
//     p.name = C.CString(cp.name)
//     p.age = (C.int)(cp.age)
//     return p
// }

// func NewCPerson(ptr unsafe.Pointer) CPerson {
//     var cp CPerson
//     p := (*C.Person)(ptr)
//     cp.name = C.GoString(p.name)
//     cp.age = (int)(p.age)
//     return cp
// }

// func PrintPerson() {
// 	p := GetPerson()
// 	fmt.Println("name:", C.GoString(p.name))
// 	fmt.Println("age:", p.age)
// }

// func GetPerson() CPerson {
// 	p := NewCPerson(unsafe.Pointer(C.new_person()))
// 	p.name = C.CString("John Doe")
// 	p.age = 39
// 	return p
// }

// package main

// /*
// #include "stdlib.h"
// #include "stdint.h"
// #include "stdio.h"
// 	typedef struct FileInfo{
// 		char 	*Name;
// 		char 	*Path;
// 	}FileInfo;

// 	typedef struct Result{
// 		FileInfo **files;
// 	}Result;
// */
// import "C"
// import (
// 	"unsafe"
// )

// func GetValue() {
// var aiList []*C.struct_FileInfo

// aif := &C.struct_FileInfo{}
// aif = (*C.struct_FileInfo)(C.malloc(C.size_t(unsafe.Sizeof(C.struct_FileInfo{}))))

// aif.Name = C.CString("abcd")
// aif.Path = C.CString("/path")
// aiList = append(aiList, aif)

// air := &C.struct_Result{}
// air = (*C.struct_Result)(C.malloc(C.size_t(unsafe.Sizeof(C.struct_Result{}))))
// air.files = &aiList[0]

// ptrAddr := *((*C.int64_t)(unsafe.Pointer(&air)))

// exportToFFI(ptrAddr)
// }

// func FreeMemory(pointer *int64, totalFiles int64){
// 	air := (*C.struct_Result)(unsafe.Pointer(pointer))
// 	files := (*[1 << 30]C.struct_FileInfo)(unsafe.Pointer(*air.files))[:totalFiles]

// 	for _, item := range files {
// 		C.free(unsafe.Pointer(&item.Name))
// 		C.free(unsafe.Pointer(&item.Path))
// 		C.free(unsafe.Pointer(&item))
// 	}
// }

// typedef struct PayloadFile {
//     int64_t Digest;
//     char* FilePath;
//     bool Exists;
// } PayloadFile;
// typedef struct PayloadFileList{
// 	PayloadFile **files;
// 	int n;
// } PayloadFileList;
