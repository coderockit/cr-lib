package model

import "testing"

func TestCloseCHandles(t *testing.T) {
	// returnCode := FindFilesWithPayloads(
	// 	"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/src",
	// 	"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/.coderockit",
	// 	int(logLevel), []string{".git", "node_modules"}, []string{}, []string{}, []string{}, []string{})
	returnCode := CloseCHandles()
	if returnCode != 0 {
		t.Fatalf("FindFilesWithPayloads had non-zero return code: %d", returnCode)
	}
}
