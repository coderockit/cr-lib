package model

/*
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
*/
import "C"

import (
	"runtime/cgo"
)

type HandleType int

const (
	PayloadFile HandleType = iota
	PayloadFileArray
	Payload
	PayloadArray
)

type cHandleManager struct {
	chandles map[uint64]HandleType
}

func (m *cHandleManager) addCgoHandle(cgohandle cgo.Handle, handleType HandleType) {
	m.chandles[uint64(C.uintptr_t(cgohandle))] = handleType
}

var singletonCHandleMananger cHandleManager = cHandleManager{chandles: make(map[uint64]HandleType)}

func CloseCHandles() int {
	// loop over singletonCHandleMananger.chandles map and close objects in map
	return 0
}

func NewCgoHandle(obj any, objType HandleType) cgo.Handle {
	cgohandle := cgo.NewHandle(obj)
	singletonCHandleMananger.addCgoHandle(cgohandle, objType)
	return cgohandle
}

func GetCgoHandle(chandle uint64) cgo.Handle {
	return cgo.Handle(chandle)
}

func GetCgoHandleType(chandle uint64) HandleType {
	return singletonCHandleMananger.chandles[chandle]
}

func DeleteCHandle(chandle uint64) {
	delete(singletonCHandleMananger.chandles, chandle)
}

// func GetNextPart(chandle uint64, chandleType HandleType) int {
// 	cgoHandle := cgo.Handle(chandle)
// 	obj := cgoHandle.Value().(*sql.Rows)
// 	if chandleType == PayloadFileArray {
// 		if obj.Next() {
// 			return GetNextPayloadFile(obj)
// 		} else {
// 			return 0
// 		}
// 	}
// 	return 0
// }
