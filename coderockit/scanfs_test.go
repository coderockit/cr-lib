package coderockit

import (
	"fmt"
	"math"
	"testing"

	"gitlab.com/coderockit/cr-lib/coderockit/gizmo"
)

var logLevel = gizmo.DEBUG

func TestScanfs(t *testing.T) {
	returnCode := FindFilesWithPayloads(
		"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/src",
		"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/.coderockit",
		int(logLevel), []string{".git", "node_modules"}, []string{}, []string{}, []string{}, []string{})
	if returnCode != 0 {
		t.Fatalf("FindFilesWithPayloads had non-zero return code: %d", returnCode)
	}
}

func TestScanfsWithNil(t *testing.T) {
	returnCode := FindFilesWithPayloads(
		"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/src",
		"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/.coderockit",
		int(logLevel), nil, nil, nil, []string{}, nil)
	if returnCode != 0 {
		t.Fatalf("FindFilesWithPayloads had non-zero return code: %d", returnCode)
	}
}

func TestScanfsWithRegExp(t *testing.T) {
	returnCode := FindFilesWithPayloads(
		"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/src",
		"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/.coderockit",
		int(logLevel), []string{".git", "node_modules"}, []string{}, []string{"dart"}, []string{}, []string{})
	if returnCode != 0 {
		t.Fatalf("FindFilesWithPayloads had non-zero return code: %d", returnCode)
	}
}

func TestScanfsList(t *testing.T) {
	fileList := GetScannedFilesList(
		"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/src",
		"/Users/normanjarvis/forge/personal/code/coderockit/cr-cli/test/.coderockit",
		int(logLevel), []string{".git", "node_modules"}, []string{}, []string{}, []string{}, math.MinInt64)
	fmt.Printf("file list handle: %v\n", fileList)
	//fileList.PrintFiles()
	// for listIndex, payloadFile := range fileList.Files {
	// 	fmt.Printf("Item [%d] had value: {%d, %s, %v}\n", listIndex, payloadFile.Digest, payloadFile.FilePath, payloadFile.Exists)
	// }
	// fmt.Printf("Item [%d] had value: {%d, %s, %v}\n", 0, fileList.Files.digest, payloadFile.FilePath, payloadFile.Exists)
}
